**Ansible playbooks to setup a Raspberry Pi with FHEM and Enocean Pi.**

## Developed and tested with the following setup:
* Ubuntu 17.10 + Ansible 2.4.0.0
* Raspberry Pi Model B Rev 2
* Raspbian Stretch Lite, Version: November 2017

## Install FHEM on top of Raspbian and basic configuration steps:

### Prepare the control machine
1. Install Ansible. Instructions are for Ubuntu, taken from [here](http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-ubuntu)
```
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible
```
1. Clone this repository:
```
git clone https://gitlab.com/habercde/pi-fhem-config-enocean-ansible.git
cd pi-fhem-config-enocean-ansible
```

### Write Raspbian image to SD card
The playbook ```pi_wrt_img_local.yaml``` will execute on your local machine.
It needs the device name of the SD card as input and it asks for your sudo password.
It unmounts all partitions of the given device, downloads the latest raspbian image and writes it to the given device.
1. Run ```df -h``` to see which devices are currently mounted.
1. If your computer has a slot for SD cards, insert the card. If not, insert the card into an SD card reader, then connect the reader to your computer.
1. Run ```df -h``` again. The new device that has appeared is your SD card. 
You can find longer instructions [here](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md).
1. Replace ```/dev/sdX``` with your SD cards device name and run this command:
```
ansible-playbook -i ./ansible/inventory ./ansible/pi_wrt_img_local.yaml --extra-vars "device=/dev/sdX" --ask-become-pass
```
1. Unplug your SD card and insert it again
1. Check the output of ```df -h``` to double check the device name of your SD Card
1. To enable SSH and configure WLAN, replace ```/dev/sdX``` with your SD cards device name and run this command:
```
ansible-playbook -i ./ansible/inventory ./ansible/pi_en_ssh_wifi_local.yaml --extra-vars "device=/dev/sdX"
```

The SD card is now ready to be plugged into the Raspberry Pi. The Raspberry Pi should be able to connect with the configured WiFi and it should accept ssh connections.

### Basic configuration steps and installation of FHEM  

Configure your WiFi access point to assign a permanent IP address to your Rasberry Pi, update the inventory file within the ./ansible directory with that IP address
and now you can use ansible playbooks to configure your Raspberry Pi.
1. Generate and copy ssh key of user pi to Raspberry Pi
1. Disable console over serial interface
```
ansible-playbook -i ./ansible/inventory ./ansible/pi_disable_serial_console.yaml
```
1. Install FHEM
```
ansible-playbook -i ./ansible/inventory ./ansible/pi_install_fhem.yaml
```